/**
 * 预约管理管理初始化
 */
var Subscribe = {
    id: "SubscribeTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
Subscribe.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: 'id', field: 'subscribeId', visible: true, align: 'center', valign: 'middle'},
            {title: '学号', field: 'studentNumber', visible: true, align: 'center', valign: 'middle'},
            {title: '学员', field: 'studentName', visible: true, align: 'center', valign: 'middle'},
            {title: '课程', field: 'courseName', visible: true, align: 'center', valign: 'middle'},
            {title: '预约时间', field: 'subscribeTime', visible: true, align: 'center', valign: 'middle'},
            {title: '预约状态', field: 'subscribeStatusText', visible: true, align: 'center', valign: 'middle'},
            {title: '打卡时间', field: 'signTime', visible: true, align: 'center', valign: 'middle'}
    ];
};

/**
 * 检查是否选中
 */
Subscribe.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        Subscribe.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加预约管理
 */
Subscribe.openAddSubscribe = function () {
    var index = layer.open({
        type: 2,
        title: '添加预约管理',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/subscribe/subscribe_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看预约管理详情
 */
Subscribe.openSubscribeDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '预约管理详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/subscribe/subscribe_update/' + Subscribe.seItem.subscribeId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除预约管理
 */
Subscribe.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/subscribe/delete", function (data) {
            Feng.success("删除成功!");
            Subscribe.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("subscribeId",this.seItem.id);
        ajax.start();
    }
};

/**
 * 查询预约管理列表
 */
Subscribe.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    Subscribe.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = Subscribe.initColumn();
    var table = new BSTable(Subscribe.id, "/subscribe/list", defaultColunms);
    table.setPaginationType("client");
    Subscribe.table = table.init();
});
