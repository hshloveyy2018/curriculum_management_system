/**
 * 初始化预约管理详情对话框
 */
var SubscribeInfoDlg = {
    subscribeInfoData : {}
};

/**
 * 清除数据
 */
SubscribeInfoDlg.clearData = function() {
    this.subscribeInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SubscribeInfoDlg.set = function(key, val) {
    this.subscribeInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
SubscribeInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
SubscribeInfoDlg.close = function() {
    parent.layer.close(window.parent.Subscribe.layerIndex);
}

/**
 * 收集数据
 */
SubscribeInfoDlg.collectData = function() {
    this
    .set('subscribeId')
    .set('memberId')
    .set('courseId')
    .set('createTime')
    .set('subscribeTime')
    .set('updateTime')
    .set('subscribeStatus')
    .set('signTime');
}

/**
 * 提交添加
 */
SubscribeInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/subscribe/add", function(data){
        Feng.success("添加成功!");
        window.parent.Subscribe.table.refresh();
        SubscribeInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.subscribeInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
SubscribeInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/subscribe/update", function(data){
        Feng.success("修改成功!");
        window.parent.Subscribe.table.refresh();
        SubscribeInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.subscribeInfoData);
    ajax.start();
}

$(function() {

});
