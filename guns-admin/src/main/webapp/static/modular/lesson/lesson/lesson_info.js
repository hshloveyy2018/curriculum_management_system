/**
 * 初始化课程管理详情对话框
 */
var LessonInfoDlg = {
    lessonInfoData : {}
};

/**
 * 清除数据
 */
LessonInfoDlg.clearData = function() {
    this.lessonInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LessonInfoDlg.set = function(key, val) {
    this.lessonInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
LessonInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
LessonInfoDlg.close = function() {
    parent.layer.close(window.parent.Lesson.layerIndex);
}

/**
 * 收集数据
 */
LessonInfoDlg.collectData = function() {
    this
    .set('lessonId')
    .set('lessonName');
}

/**
 * 提交添加
 */
LessonInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();
    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/lesson/add", function(data){
        Feng.success("添加成功!");
        window.parent.Lesson.table.refresh();
        LessonInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.lessonInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
LessonInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/lesson/update", function(data){
        Feng.success("修改成功!");
        window.parent.Lesson.table.refresh();
        LessonInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.lessonInfoData);
    ajax.start();
}

$(function() {

});
