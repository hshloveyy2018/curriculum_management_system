/**
 * 初始化课程时间详情对话框
 */
var CourseTimeInfoDlg = {
    courseTimeInfoData : {}
};

/**
 * 清除数据
 */
CourseTimeInfoDlg.clearData = function() {
    this.courseTimeInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CourseTimeInfoDlg.set = function(key, val) {
    this.courseTimeInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CourseTimeInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
CourseTimeInfoDlg.close = function() {
    parent.layer.close(window.parent.CourseTime.layerIndex);
}

/**
 * 收集数据
 */
CourseTimeInfoDlg.collectData = function() {
    this
    .set('courseTimeId')
    .set('name')
    .set('courseStartTime')
    .set('courseEndTime')
    .set('createTime')
    .set('updateTime');
}

/**
 * 提交添加
 */
CourseTimeInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/courseTime/add", function(data){

        Feng.success("添加成功!");
        window.parent.CourseTime.table.refresh();
        CourseTimeInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.courseTimeInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
CourseTimeInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/courseTime/update", function(data){
        Feng.success("修改成功!");
        window.parent.CourseTime.table.refresh();
        CourseTimeInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.courseTimeInfoData);
    ajax.start();
}

$(function() {

});
