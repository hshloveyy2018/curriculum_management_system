/**
 * 课程时间管理初始化
 */
var CourseTimeEdit = {
    id: "CourseTimeEditTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
CourseTimeEdit.initColumn = function (lessonList, timeList) {

    var selectHtml = '<select>';
    selectHtml += '<option></option>';

    var optionsHtml = '';
    for (let i = 0; i < lessonList.length; i++) {
        selectHtml += '<option value="' + lessonList[i].lessonId + '">' + lessonList[i].lessonName + '</option>';
        optionsHtml += '<option value="' + lessonList[i].lessonId + '">' + lessonList[i].lessonName + '</option>';
    }
    selectHtml += '</select>';

    var arr = [{
        title: 'id',
        field: 'courseDayId',
        visible: false,
        align: 'center',
        valign: 'middle'
    }, {
        title: '',
        field: 'courseDay',
        visible: true,
        width: '10%',
        align: 'center',
        valign: 'middle'
    }];
    for (let i = 0; i < timeList.length; i++) {
        arr.push({
            title: timeList[i].name,
            field: 'lessonId' + i,
            visible: true,
            align: 'center',
            valign: 'middle',
            formatter: function (value, row, index) {
                var optionsHtml = '';
                for (let i = 0; i < lessonList.length; i++) {
                    optionsHtml += '<option value="' + lessonList[i].lessonId + '" ' + (value == lessonList[i].lessonId ? 'selected="selected"' : '') + '>' + lessonList[i].lessonName + '</option>';
                }
                return '<select data-col="' + i + '"><option value="0">请选择</option>' + optionsHtml + '</select>';
            }
        });
    }

    return arr;
};

CourseTimeEdit.addSubmit = function (weekId) {

    let data = this.getData();

    var ajax = new $ax(Feng.ctxPath + "/courseTime/setCourseTime", function (response) {
        CourseTimeInfoDlg.close();
        Feng.success('保存成功');
    }, function (response) {
        Feng.error("查询异常");
    });

    let courseTime = [];
    for (let i = 0; i < data.length; i++) {
        let str = '';
        for (let j = 0; j < data[i].length; j++) {
            str += '|' + data[i][j];
        }
        str = (i + 1) + '@' + str;
        courseTime.push(str);
    }

    let s = '';
    for (let i = 0; i < courseTime.length; i++) {
        if (i == 0) {
            s += courseTime[i];
        } else {
            s += ',' + courseTime[i];
        }
    }

    let params = {
        courseTime: s,
        weekId: weekId
    };
    ajax.set(params);
    ajax.start();

};


CourseTimeEdit.getData = function () {

    var data = new Array();
    var row = 0;
    $('select').each(function (index, item) {
        // if (!$(item).val()) {
        //     Feng.error('还有未安排的课程');
        //     return;
        // }

        if (!data[row]) {
            data[row] = new Array();
        }
        data[row][$(item).data("col")] = $(item).val();

        if ((index + 1) % 4 == 0) {
            row += 1;
        }
    });

    return data;
};


$(function () {

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/courseTime/courseTime", function (data) {


        var defaultColunms = CourseTimeEdit.initColumn(data.lessonList, data.timeList);
        // var table = new BSTable(CourseTime.id, "/courseTime/list", defaultColunms);
        // CourseTime.table = table.init();

        let columnData = [];

        // first column data
        if (data.courseList.length) {
            for (let i = 0; i < data.weekList.length; i++) {
                columnData.push({
                    courseDayId: data.weekList[i].code,
                    courseDay: data.weekList[i].name,
                    lessonId0: data.courseList[i * 4].lessonId,
                    lessonId1: data.courseList[i * 4 + 1].lessonId,
                    lessonId2: data.courseList[i * 4 + 2].lessonId,
                    lessonId3: data.courseList[i * 4 + 3].lessonId
                });
            }
        } else {
            for (let i = 0; i < data.weekList.length; i++) {
                columnData.push({
                    courseDayId: data.weekList[i].code,
                    courseDay: data.weekList[i].name
                });
            }
        }


        $('#CourseTimeEditTable').bootstrapTable({
            checkboxHeader: false,
            data: columnData,
            striped: true,     			//是否显示行间隔色
            sortable: false,      		//是否启用排序
            sortOrder: "desc",     		//排序方式
            search: false,      		//是否显示表格搜索，此搜索是客户端搜索，不会进服务端
            strictSearch: true,			//设置为 true启用 全匹配搜索，否则为模糊搜索
            showColumns: false,     		//是否显示所有的列
            showRefresh: false,         //是否显示刷新按钮
            minimumCountColumns: 2,    	//最少允许的列数
            clickToSelect: false,    	//是否启用点击选中行
            searchOnEnterKey: true,		//设置为 true时，按回车触发搜索方法，否则自动触发搜索方法
            columns: defaultColunms,		//列数组
            pagination: false,			//是否显示分页条
            height: this.height,
            icons: {
                refresh: 'glyphicon-repeat',
                toggle: 'glyphicon-list-alt',
                columns: 'glyphicon-list'
            },
            iconSize: 'outline'
        });

    }, function (data) {
        Feng.error("查询异常");
    });
    ajax.set("weekId", $('#weekId').val());
    ajax.start();
});
