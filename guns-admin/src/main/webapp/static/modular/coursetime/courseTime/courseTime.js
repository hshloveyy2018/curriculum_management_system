/**
 * 课程时间管理初始化
 */
var CourseTime = {
    id: "CourseTimeTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1
};

/**
 * 初始化表格的列
 */
CourseTime.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
            {title: 'id', field: 'courseTimeId', visible: false, align: 'center', valign: 'middle'},
            {title: '名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '开始日期', field: 'courseStartTime', visible: true, align: 'center', valign: 'middle'},
            {title: '结束日期', field: 'courseEndTime', visible: true, align: 'center', valign: 'middle'},
            {title: '操作', field: 'courseTimeId', visible: true, align: 'center', valign: 'middle', formatter: function (value, row, index) {
                var s = '<a href="javascript:CourseTime.setLesson(' + value + ');">编辑课表</a>';
                if(!row.status){
                    s += '|<a href="javascript:CourseTime.apply(' + value + ');">发布</a>';
                }else{
                    s += '| 已发布';
                }
                    return s;
                }}
    ];
};

CourseTime.setLesson = function (value) {
    var index = layer.open({
        type: 2,
        title: '编辑课程表',
        area: ['900px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/courseTime/toCourseTime?weekId=' + value
    });
    this.layerIndex = index;
};


CourseTime.apply = function (value) {
    var ajax = new $ax(Feng.ctxPath + "/courseTime/apply", function(response){
        Feng.success("发布成功");

        CourseTime.table.refresh();
    },function(response){
        Feng.error("发布失败");
    });
    ajax.set("courseTimeId", value);
    ajax.start();
};

/**
 * 检查是否选中
 */
CourseTime.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if(selected.length == 0){
        Feng.info("请先选中表格中的某一记录！");
        return false;
    }else{
        CourseTime.seItem = selected[0];
        return true;
    }
};

/**
 * 点击添加课程时间
 */
CourseTime.openAddCourseTime = function () {
    var index = layer.open({
        type: 2,
        title: '添加课程时间',
        area: ['800px', '420px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/courseTime/courseTime_add'
    });
    this.layerIndex = index;
};

/**
 * 打开查看课程时间详情
 */
CourseTime.openCourseTimeDetail = function () {
    if (this.check()) {
        var index = layer.open({
            type: 2,
            title: '课程时间详情',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/courseTime/courseTime_update/' + CourseTime.seItem.courseTimeId
        });
        this.layerIndex = index;
    }
};

/**
 * 删除课程时间
 */
CourseTime.delete = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/courseTime/delete", function (data) {
            Feng.success("删除成功!");
            CourseTime.table.refresh();
        }, function (data) {
            Feng.error("删除失败!" + data.responseJSON.message + "!");
        });
        ajax.set("courseTimeId",this.seItem.courseTimeId);
        ajax.start();
    }
};

/**
 * 查询课程时间列表
 */
CourseTime.search = function () {
    var queryData = {};
    queryData['condition'] = $("#condition").val();
    CourseTime.table.refresh({query: queryData});
};

$(function () {
    var defaultColunms = CourseTime.initColumn();
    var table = new BSTable(CourseTime.id, "/courseTime/list", defaultColunms);
    table.setPaginationType("client");
    CourseTime.table = table.init();
});
