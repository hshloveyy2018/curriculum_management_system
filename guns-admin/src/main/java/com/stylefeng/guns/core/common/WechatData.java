package com.stylefeng.guns.core.common;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class WechatData {

    /**
     * {
     *      "access_token":"13_r-KA4q37dTUVagtS6MAguiY9K4DQR-tIFXmggb_6fqf32Lx7vGJ8SVruyaU5-YQRtEkeLoZ_bkk_UqtexOK0yQ",
     *      "expires_in":7200,
     *      "refresh_token":"13_E4RIonniuAFWjyNYl0ZJB5kC34y1Ty0INMqsndilJ2NZ9kk9UY_GnMEHpxKJ89btSP-aAwOuK4B2LRSi7jjdWQ",
     *      "openid":"omieRuHmqu03qXjC8kXLDiMpRG38",
     *      "scope":"snsapi_userinfo"
     * }
     */

    public static String accessToken;

    public static Integer expiresIn;

    public static String openid;

    public static String refreshToken;


    /**
     * 绑定成功通知
     */
    public static final String BIND_SUCCESS = "4axo94Q4ocldyD8tWnMllE7-BqaPyBqasZBaJEmjFi0";
    /**
     * 预约课程成功通知
     */
    public static final String SUBSCRIBE_SUCCESS = "vUZiOSfGRUvAIDH8ViVxiEGHTGpo1crX5ECS2FQfd50";
    /**
     * 绑定成功通知
     */
    public static final String SUBSCRIBE_CANCEL = "M8af-I3Tvfjz285tMpcloYttrjow0g7NP2VU2-s_yiU";
    /**
     * 绑定成功通知
     */
    public static final String SIGN_SUCCESS = "M3Sbwmq1C7qtDLtTcTpsR1-V8zRUydgIyP9_bMqAfPI";


    public static void set(String data){
        JSONObject json = JSON.parseObject(data);
        accessToken = json.getString("access_token");
        expiresIn = json.getInteger("expires_in");
        refreshToken = json.getString("refresh_token");
        openid = json.getString("openid");
    }
}
