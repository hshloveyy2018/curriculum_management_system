package com.stylefeng.guns.core.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;
import com.stylefeng.guns.core.common.AccessToken;
import com.stylefeng.guns.core.common.WechatData;

public class WechatUtils {

    private static final String APP_ID = "wxacf98090e3c90e8b";
    private static final String APP_SECRET = "1900d8669ca945b7830c2001cf0cf44b";

    private static final String OAUTH2_ACCESS_TOKEN = "https://api.weixin.qq.com/sns/oauth2/access_token";

    private static final String USER_INFO = "https://api.weixin.qq.com/sns/userinfo";

    private static final String REFRESH_TOKEN = "https://api.weixin.qq.com/sns/oauth2/refresh_token";

    private static final String MESSAGE_TEMPLATE = "https://api.weixin.qq.com/cgi-bin/message/template/send";

    private static final String TOKEN = "https://api.weixin.qq.com/cgi-bin/token";


    /**
     * 通过code换取网页授权access_token
     *
     * @return
     */
    public static String getAccessTokenByCode(String code) {
        if (StringUtils.isBlank(WechatData.accessToken)) {
            Map<String, String> params = new HashMap<>();
            params.put("appid", APP_ID);
            params.put("secret", APP_SECRET);
            params.put("code", code);
            params.put("grant_type", "authorization_code");

            String result = SimpleHttpUtils.URLGet(OAUTH2_ACCESS_TOKEN, params, "UTF-8");
            System.out.println(result);
            WechatData.set(result);
        }
        return WechatData.accessToken;
    }

    public static String token(){
        Map<String, String> params = new HashMap<>();
        params.put("grant_type", "client_credential");
        params.put("appid", APP_ID);
        params.put("secret", APP_SECRET);
        String result = SimpleHttpUtils.URLGet(TOKEN, params, "UTF-8");
        System.out.println(result);
        AccessToken.token = JSON.parseObject(result).getString("access_token");
        return AccessToken.token;
    }

    /**
     * 网页授权获取用户基本信息
     *
     * @return
     */
    public static String getUserInfo() {
        Map<String, String> params = new HashMap<>();
        params.put("access_token", WechatData.accessToken);
        params.put("openid", WechatData.openid);
        params.put("lang", "zh_CN");
        String result = SimpleHttpUtils.URLGet(USER_INFO, params, "UTF-8");
        System.out.println(result);
        return result;
    }

    /**
     * 网页授权获取用户基本信息
     *
     * @return
     */
    public static void refreshAccessToken(String refreshToken) {
        Map<String, String> params = new HashMap<>();
        params.put("appid", APP_ID);
        params.put("refresh_token", refreshToken);
        params.put("grant_type", "refresh_token");

        String result = SimpleHttpUtils.URLGet(REFRESH_TOKEN, params, "UTF-8");

        WechatData.set(result);
    }

    /**
     * 发送模板消息
     */
    public static void sendTemplateMessage(String templateId, String openid, String url, Map<String, Object> data) {
        if(StringUtils.isBlank(AccessToken.token)){
            token();
        }
        Map<String, Object> params = new HashMap<>();
        params.put("touser", openid);
        params.put("template_id", templateId);
        params.put("url", url);
//        params.put("data", JSON.toJSONString(data));
        params.put("data", data);
//        params.put("access_token", AccessToken.token);
//        + "?access_token=" + AccessToken.token
        String result = SimpleHttpUtils.URLJson(MESSAGE_TEMPLATE + "?access_token=" + AccessToken.token, params);
        System.out.println(result);
    }

    //https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxacf98090e3c90e8b&redirect_uri=http://www.91mingyang.cn/wechat/&response_type=code&scope=snsapi_userinfo#wechat_redirect

}
