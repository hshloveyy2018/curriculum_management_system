package com.stylefeng.guns.core.schedule;

import com.stylefeng.guns.core.common.WechatData;
import com.stylefeng.guns.core.util.WechatUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class WechatAccessTokenSchedule {

    /**
     * 5分钟刷新微信网页授权accessToken
     */
    @Scheduled(fixedRate = 200000)
    public void run() {
        if(StringUtils.isNotBlank(WechatData.refreshToken)){
            WechatUtils.refreshAccessToken(WechatData.refreshToken);
        }
    }

    @Scheduled(fixedRate = 3600000)
    public void accessTokenCenter(){
        String tokenString = WechatUtils.token();
        System.out.println(tokenString);
    }
}
