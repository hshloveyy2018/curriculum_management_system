package com.stylefeng.guns.core.common.exception;

import com.stylefeng.guns.core.exception.GunsException;
import com.stylefeng.guns.core.exception.ServiceExceptionEnum;

/**
 * 验证码错误异常
 *
 * @author fengshuonan
 * @date 2017-05-05 23:52
 */
public class APIException extends GunsException {

    public APIException(ServiceExceptionEnum serviceExceptionEnum) {
        super(serviceExceptionEnum);
    }
}
