package com.stylefeng.guns.modular.student.service;

import com.stylefeng.guns.modular.system.model.Member;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author stylefeng
 * @since 2018-08-22
 */
public interface IMemberService extends IService<Member> {

}
