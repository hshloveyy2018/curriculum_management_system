package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.Course;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author heshaohua
 * @since 2018-08-22
 */
public interface CourseMapper extends BaseMapper<Course> {

}
