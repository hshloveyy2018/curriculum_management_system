package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.Member;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author stylefeng
 * @since 2018-08-22
 */
public interface MemberMapper extends BaseMapper<Member> {

}
