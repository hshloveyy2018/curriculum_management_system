package com.stylefeng.guns.modular.coursetime.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.modular.course.service.ICourseService;
import com.stylefeng.guns.modular.lesson.service.ILessonService;
import com.stylefeng.guns.modular.subscribe.service.ISubscribeService;
import com.stylefeng.guns.modular.system.model.*;
import com.stylefeng.guns.modular.system.service.IDictService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.coursetime.service.ICourseTimeService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 课程时间控制器
 *
 * @author fengshuonan
 * @Date 2018-08-29 21:25:23
 */
@Controller
@RequestMapping("/courseTime")
public class CourseTimeController extends BaseController {

    private String PREFIX = "/coursetime/courseTime/";

    @Autowired
    private ICourseTimeService courseTimeService;

    @Autowired
    private ICourseService courseService;

    @Autowired
    private ILessonService lessonService;

    @Autowired
    private IDictService dictService;

    @Autowired
    private ISubscribeService subscribeService;

    /**
     * 跳转到课程时间首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "courseTime.html";
    }

    /**
     * 跳转到添加课程时间
     */
    @RequestMapping("/courseTime_add")
    public String courseTimeAdd() {
        return PREFIX + "courseTime_add.html";
    }

    /**
     * 跳转到修改课程时间
     */
    @RequestMapping("/courseTime_update/{courseTimeId}")
    public String courseTimeUpdate(@PathVariable Integer courseTimeId, Model model) {
        CourseTime courseTime = courseTimeService.selectById(courseTimeId);
        model.addAttribute("item", courseTime);
        LogObjectHolder.me().set(courseTime);
        return PREFIX + "courseTime_edit.html";
    }

    /**
     * 获取课程时间列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return courseTimeService.selectList(null);
    }

    /**
     * 新增课程时间
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(CourseTime courseTime) {
        courseTimeService.insert(courseTime);
        return SUCCESS_TIP;
    }

    /**
     * 删除课程时间
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer courseTimeId) {
        courseTimeService.deleteById(courseTimeId);

        courseService.delete(new EntityWrapper<>(new Course()).eq("week_id", courseTimeId));
        return SUCCESS_TIP;
    }

    /**
     * 修改课程时间
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(CourseTime courseTime) {
        courseTimeService.updateById(courseTime);
        return SUCCESS_TIP;
    }

    /**
     * 课程时间详情
     */
    @RequestMapping(value = "/detail/{courseTimeId}")
    @ResponseBody
    public Object detail(@PathVariable("courseTimeId") Integer courseTimeId) {
        return courseTimeService.selectById(courseTimeId);
    }

    /**
     * 跳转到添加课程时间
     */
    @RequestMapping("/toCourseTime")
    public String toCourseTime(Integer weekId, Model model) {
        model.addAttribute("weekId", weekId);
        return PREFIX + "courseTimeEdit.html";
    }

    /**
     * 课程时间
     */
    @ResponseBody
    @RequestMapping(value = "/courseTime")
    public Object courseTime(Integer weekId) {

        Map<String, Object> result = new HashMap<>();

        List<Lesson> list = lessonService.selectList(null);
        result.put("lessonList", list);

        Dict dict = new Dict();
        dict.setCode("CLASS_TIME");
        Dict parentDict = dictService.selectOne(new EntityWrapper(dict));
        dict.setCode(null);
        dict.setPid(parentDict.getId());
        List<Dict> timeList = dictService.selectList(new EntityWrapper(dict));
        result.put("timeList", timeList);

        List<Dict> weekList = courseService.getWeekDayList();
        result.put("weekList", weekList);

        Course course = new Course();
        course.setWeekId(weekId);
        List<Course> courseList = courseService.selectList(new EntityWrapper(course).orderBy("course_day", true).orderBy("course_time_id", true));
        result.put("courseList", courseList);
        return result;
    }

    /**
     * 发布课程时间
     */
    @ResponseBody
    @RequestMapping(value = "/apply")
    public Object apply(Integer courseTimeId) {
        CourseTime courseTime = new CourseTime();
        courseTime.setCourseTimeId(courseTimeId);
        courseTime.setStatus(1);
        courseTimeService.updateById(courseTime);
        return SUCCESS_TIP;
    }

    /**
     * 课程时间
     */
    @ResponseBody
    @RequestMapping(value = "/setCourseTime")
    public Object setCourseTime(String courseTime, Integer weekId) {

        Map<Integer, String> map = new HashMap<>();
        List<Lesson> lessons = lessonService.selectList(null);
        for (int i = 0; i < lessons.size(); i++) {
            map.put(lessons.get(i).getLessonId(), lessons.get(i).getLessonName());
        }

        List<Course> list = new ArrayList<>();
        String[] weekDayArr = courseTime.split(",");
        for (int i = 0; i < weekDayArr.length; i++) {

            String[] timeArr = weekDayArr[i].split("@");
            String weekDay = timeArr[0];

            String[] courseArr = timeArr[1].split("\\|");

            for (int j = 1; j < courseArr.length; j++) {

                Course course = new Course();

                course.setCourseDay(Integer.valueOf(weekDay));
                course.setCourseTimeId(j);
                course.setWeekId(weekId);

                Course sourceCourse = courseService.selectOne(new EntityWrapper<>(course));

                if(sourceCourse != null){
                    sourceCourse.setLessonId(Integer.valueOf(courseArr[j]));
                    sourceCourse.setLessonName(map.get(sourceCourse.getLessonId()));
                    courseService.updateById(sourceCourse);

                    //取消已经被预约的记录
                    Subscribe subscribe = new Subscribe();
                    subscribe.setCourseId(sourceCourse.getCourseId());
                    subscribeService.delete(new EntityWrapper<>());
                }else{
                    course.setLessonId(Integer.valueOf(courseArr[j]));
                    list.add(course);
                }
            }

        }

        for (int i = 0; i < list.size(); i++) {
            list.get(i).setLessonName(map.get(list.get(i).getLessonId()));
        }

        if(!list.isEmpty()) {
            courseService.insertBatch(list);
        }
        return SUCCESS_TIP;
    }
}
