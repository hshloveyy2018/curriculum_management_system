package com.stylefeng.guns.modular.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.util.JwtTokenUtil;
import com.stylefeng.guns.core.util.WechatUtils;
import com.stylefeng.guns.modular.student.service.IMemberService;
import com.stylefeng.guns.modular.system.model.Member;

/**
 * 登录控制器
 *
 * @author heshaohua
 * @Date 2018年08月26日11:49:50
 */
@Controller
@RequestMapping("/wechat")
public class WechatController extends BaseController {

    @Autowired
    private IMemberService memberService;

    /**
     * 微信登陆
     */
    @SuppressWarnings("unused")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String wechat(String code, Model model) {
        String result = WechatUtils.getAccessTokenByCode(code);
        String userInfo = WechatUtils.getUserInfo();

        if (!userInfo.contains("openid")) {
            return "redirect:https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxacf98090e3c90e8b&redirect_uri=http://www.thetateach.cn/wechat/&response_type=code&scope=snsapi_userinfo#wechat_redirect";
        }

        JSONObject userJson = JSON.parseObject(userInfo);
        // 查询数据库中是否存在openid
        Member member = new Member();
        member.setOpenid(userJson.getString("openid"));
        Member wechatUser = memberService.selectOne(new EntityWrapper<>(member));

        if (wechatUser != null) {// 用户已经绑定微信
            // 后台登陆

            model.addAttribute("token", JwtTokenUtil.generateToken(member.getStudentNumber()));
            wechatUser.setPassword(null);
            wechatUser.setSalt(null);

            model.addAttribute("user", JSON.toJSONString(wechatUser));
            return "/wechat/index.html";
        } else {// 跳转到绑定微信页面
            // request.getSession().setAttribute("code", code);
            model.addAttribute("code", code);
            model.addAttribute("wechatUser", userJson);
            return "/wechat/bind.html";
        }
    }

    /**
     * 跳转到登录页面
     */
    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    public String login(@PathVariable("name") String name) {
        return "/wechat/" + name + ".html";
    }

}
