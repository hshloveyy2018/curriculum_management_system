package com.stylefeng.guns.modular.course.service;

import com.stylefeng.guns.modular.system.model.Course;
import com.baomidou.mybatisplus.service.IService;
import com.stylefeng.guns.modular.system.model.Dict;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author heshaohua
 * @since 2018-08-22
 */
public interface ICourseService extends IService<Course> {

    String getWeekDay(Integer weekId, Integer courseDay);

    List<Dict> getWeekDayList();
}
