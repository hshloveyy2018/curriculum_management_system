package com.stylefeng.guns.modular.lesson.service.impl;

import com.stylefeng.guns.modular.system.model.Lesson;
import com.stylefeng.guns.modular.system.dao.LessonMapper;
import com.stylefeng.guns.modular.lesson.service.ILessonService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author heshaohua
 * @since 2018-08-29
 */
@Service
public class LessonServiceImpl extends ServiceImpl<LessonMapper, Lesson> implements ILessonService {

}
