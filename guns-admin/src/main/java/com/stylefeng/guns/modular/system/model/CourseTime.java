package com.stylefeng.guns.modular.system.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author heshaohua
 * @since 2018-08-29
 */
@TableName("course_time")
public class CourseTime extends Model<CourseTime> {

    private static final long serialVersionUID = 1L;

    /**
     * 课程时间
     */
    @TableId(value = "course_time_id", type = IdType.AUTO)
    private Integer courseTimeId;
    @TableField("name")
    private String name;
    @TableField("course_start_time")
    private String courseStartTime;
    @TableField("course_end_time")
    private String courseEndTime;
    @TableField("create_time")
    private Date createTime;
    @TableField("update_time")
    private Date updateTime;
    @TableId(value = "status")
    private Integer status;


    public Integer getCourseTimeId() {
        return courseTimeId;
    }

    public void setCourseTimeId(Integer courseTimeId) {
        this.courseTimeId = courseTimeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourseStartTime() {
        return courseStartTime;
    }

    public void setCourseStartTime(String courseStartTime) {
        this.courseStartTime = courseStartTime;
    }

    public String getCourseEndTime() {
        return courseEndTime;
    }

    public void setCourseEndTime(String courseEndTime) {
        this.courseEndTime = courseEndTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    protected Serializable pkVal() {
        return this.courseTimeId;
    }

    @Override
    public String toString() {
        return "CourseTime{" +
        "courseTimeId=" + courseTimeId +
        ", name=" + name +
        ", courseStartTime=" + courseStartTime +
        ", courseEndTime=" + courseEndTime +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
