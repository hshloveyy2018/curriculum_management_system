package com.stylefeng.guns.modular.web;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.google.code.kaptcha.Constants;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.base.tips.ErrorTip;
import com.stylefeng.guns.core.common.exception.BizExceptionEnum;
import com.stylefeng.guns.core.common.exception.InvalidKaptchaException;
import com.stylefeng.guns.core.exception.GunsException;
import com.stylefeng.guns.core.log.LogManager;
import com.stylefeng.guns.core.log.factory.LogTaskFactory;
import com.stylefeng.guns.core.node.MenuNode;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.core.shiro.ShiroUser;
import com.stylefeng.guns.core.util.ApiMenuFilter;
import com.stylefeng.guns.core.util.JwtTokenUtil;
import com.stylefeng.guns.core.util.KaptchaUtil;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.student.service.IMemberService;
import com.stylefeng.guns.modular.system.model.Member;
import com.stylefeng.guns.modular.system.model.User;
import com.stylefeng.guns.modular.system.service.IUserService;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

import static com.stylefeng.guns.core.support.HttpKit.getIp;

/**
 * 登录控制器
 *
 * @author heshaohua
 * @Date 2018年08月26日11:49:50
 */
@Controller
public class WebController extends BaseController {

    @Autowired
    private IMemberService memberService;

    /**
     * 跳转到主页
     */
    @RequestMapping(value = "/web", method = RequestMethod.GET)
    public String index(Model model) {

        return "/web/login.html";
    }

    /**
     * 跳转到登录页面
     */
    @RequestMapping(value = "/web/login", method = RequestMethod.GET)
    public String login() {
        if (ShiroKit.isAuthenticated() || ShiroKit.getUser() != null) {
            return REDIRECT + "/web";
        } else {
            return "/web/login.html";
        }
    }

    /**
     * 点击登录执行的动作
     */
    @ResponseBody
    @RequestMapping(value = "/web/login", method = RequestMethod.POST)
    public Object loginVali() {

        String username = super.getPara("username").trim();
        String password = super.getPara("password").trim();
//封装请求账号密码为shiro可验证的token
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(username, password.toCharArray());

        //获取数据库中的账号密码，准备比对
        Member member = new Member();
        member.setStudentNumber(username);
        member = memberService.selectOne(new EntityWrapper<>(member));

        String credentials = member.getPassword();
        String salt = member.getSalt();
        ByteSource credentialsSalt = new Md5Hash(salt);
        SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(
                new ShiroUser(), credentials, credentialsSalt, "");


        //校验用户账号密码
        HashedCredentialsMatcher md5CredentialsMatcher = new HashedCredentialsMatcher();
        md5CredentialsMatcher.setHashAlgorithmName(ShiroKit.hashAlgorithmName);
        md5CredentialsMatcher.setHashIterations(ShiroKit.hashIterations);
        boolean passwordTrueFlag = md5CredentialsMatcher.doCredentialsMatch(
                usernamePasswordToken, simpleAuthenticationInfo);

        if (passwordTrueFlag) {

            super.getSession().setAttribute("shiroUser", member);
            super.getSession().setAttribute("username", member.getStudentNumber());

            LogManager.me().executeLog(LogTaskFactory.loginLog(member.getMemberId(), getIp()));

            ShiroKit.getSession().setAttribute("sessionFlag", true);

            HashMap<String, Object> result = new HashMap<>();
            result.put("token", JwtTokenUtil.generateToken(member.getStudentNumber()));
            member.setPassword(null);
            result.put("user", member);
            return result;
        } else {
            return new ErrorTip(500, "账号密码错误！");
        }
    }

}
