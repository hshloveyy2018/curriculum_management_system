package com.stylefeng.guns.modular.api.wechat;

public class TemplateMessage {

    /**
     * 	touser 是	接收者openid
     */
    private String toUser;

    /**
     * 	template_id 是	模板ID
     */
    private String templateId;

    /**
     * url	否	模板跳转链接
     */
    private String url;

/**
    appid	是	所需跳转到的小程序appid（该小程序appid必须与发模板消息的公众号是绑定关联关系，暂不支持小游戏）
    pagepath	否	所需跳转到小程序的具体页面路径，支持带参数,（示例index?foo=bar），暂不支持小游戏
    data	是	模板数据
    color	否	模板内容字体颜色，不填默认为黑色
 */
}
