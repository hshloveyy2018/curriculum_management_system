package com.stylefeng.guns.modular.system.model;

import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author stylefeng
 * @since 2018-08-22
 */
@TableName("subscribe")
public class Subscribe extends Model<Subscribe> {

    private static final long serialVersionUID = 1L;

    private static final String[] SUBSCRIBE_STATUS_TEXT = {"取消预约", "已预约", "已上课"};
    /**
     * 主键id
     */
    @TableId(value = "subscribe_id", type = IdType.AUTO)
    private Integer subscribeId;
    /**
     * 学员id
     */
    @TableField("member_id")
    private Integer memberId;
    /**
     * 学号
     */
    @TableField("student_number")
    private String studentNumber;
    /**
     * 学员姓名
     */
    @TableField("student_name")
    private String studentName;
    /**
     * 课程id
     */
    @TableField("course_id")
    private Integer courseId;
    /**
     * 课程名
     */
    @TableField("course_name")
    private String courseName;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 预约时间
     */
    @TableField("subscribe_time")
    private Date subscribeTime;
    /**
     * 最后修改时间
     */
    @TableField("update_time")
    private Date updateTime;
    /**
     * 预约状态: 0-取消预约, 1-已预约, 2-已上课
     */
    @TableField("subscribe_status")
    private Integer subscribeStatus;
    /**
     * 打卡时间
     */
    @TableField("sign_time")
    private String signTime;


    public Integer getSubscribeId() {
        return subscribeId;
    }

    public void setSubscribeId(Integer subscribeId) {
        this.subscribeId = subscribeId;
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getSubscribeTime() {
        return subscribeTime;
    }

    public void setSubscribeTime(Date subscribeTime) {
        this.subscribeTime = subscribeTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getSubscribeStatus() {
        return subscribeStatus;
    }

    public String getSubscribeStatusText() {
        return SUBSCRIBE_STATUS_TEXT[this.subscribeStatus];
    }

    public void setSubscribeStatus(Integer subscribeStatus) {
        this.subscribeStatus = subscribeStatus;
    }

    public String getSignTime() {
        return signTime;
    }

    public void setSignTime(String signTime) {
        this.signTime = signTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.subscribeId;
    }

    @Override
    public String toString() {
        return "Subscribe{" +
                "subscribeId=" + subscribeId +
                ", memberId=" + memberId +
                ", courseId=" + courseId +
                ", createTime=" + createTime +
                ", subscribeTime=" + subscribeTime +
                ", updateTime=" + updateTime +
                ", subscribeStatus=" + subscribeStatus +
                ", signTime=" + signTime +
                "}";
    }
}
