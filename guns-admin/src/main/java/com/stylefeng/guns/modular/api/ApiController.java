package com.stylefeng.guns.modular.api;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.base.tips.ErrorTip;
import com.stylefeng.guns.core.base.tips.SuccessTip;
import com.stylefeng.guns.core.base.warpper.BaseControllerWarpper;
import com.stylefeng.guns.core.common.TemplateData;
import com.stylefeng.guns.core.common.WechatData;
import com.stylefeng.guns.core.common.exception.APIException;
import com.stylefeng.guns.core.common.exception.BizExceptionEnum;
import com.stylefeng.guns.core.exception.GunsException;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.core.shiro.ShiroUser;
import com.stylefeng.guns.core.util.JwtTokenUtil;
import com.stylefeng.guns.core.util.RenderUtil;
import com.stylefeng.guns.core.util.WechatUtils;
import com.stylefeng.guns.modular.course.service.ICourseService;
import com.stylefeng.guns.modular.student.service.IMemberService;
import com.stylefeng.guns.modular.subscribe.service.ISubscribeService;
import com.stylefeng.guns.modular.system.dao.MemberMapper;
import com.stylefeng.guns.modular.system.dao.UserMapper;
import com.stylefeng.guns.modular.system.model.*;
import com.stylefeng.guns.modular.system.service.IDictService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.Executor;

/**
 * 接口控制器提供
 *
 * @author stylefeng
 * @Date 2018/7/20 23:39
 */
@RestController
@RequestMapping("/api")
public class ApiController extends BaseController {

    @Autowired
    private IMemberService memberService;

    @Autowired
    private ICourseService courseService;

    @Autowired
    private ISubscribeService subscribeService;

    @Autowired
    private IDictService dictService;

    @Autowired
    private Executor executor;

    /**
     * api登录接口，通过账号密码获取token
     */
    @PostMapping("/login")
    public Object auth(@RequestParam("username") String username,
                       @RequestParam("password") String password) {

        //封装请求账号密码为shiro可验证的token
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(username, password.toCharArray());

        //获取数据库中的账号密码，准备比对
        Member member = new Member();
        member.setStudentNumber(username);
        member = memberService.selectOne(new EntityWrapper<>(member));

        String credentials = member.getPassword();
        String salt = member.getSalt();
        ByteSource credentialsSalt = new Md5Hash(salt);
        SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(
                new ShiroUser(), credentials, credentialsSalt, "");


        //校验用户账号密码
        HashedCredentialsMatcher md5CredentialsMatcher = new HashedCredentialsMatcher();
        md5CredentialsMatcher.setHashAlgorithmName(ShiroKit.hashAlgorithmName);
        md5CredentialsMatcher.setHashIterations(ShiroKit.hashIterations);
        boolean passwordTrueFlag = md5CredentialsMatcher.doCredentialsMatch(
                usernamePasswordToken, simpleAuthenticationInfo);

        if (passwordTrueFlag) {

            HashMap<String, Object> result = new HashMap<>();
            result.put("token", JwtTokenUtil.generateToken(member.getStudentNumber()));
            member.setPassword(null);
            member.setSalt(null);
            result.put("user", member);
            return result;
        } else {
            return new ErrorTip(500, "账号密码错误！");
        }
    }

    @PostMapping("/bind")
    public Object bindWechat(String cardNumber, String studentNumber, String openid, String mobile, String nickname) {
        if(StringUtils.isBlank(cardNumber)){
            return new ErrorTip(500, "卡号不能为空");
        }
        if(StringUtils.isBlank(studentNumber)){
            return new ErrorTip(500, "学号不能为空");
        }

        Member wrapperEntity = new Member();
        wrapperEntity.setCardNumber(cardNumber);
//        wrapperEntity.setStudentNumber(studentNumber);
        wrapperEntity = memberService.selectOne(new EntityWrapper(wrapperEntity));

        if(wrapperEntity == null){
            return new ErrorTip(500, "卡号不存在");
        }

        if(wrapperEntity != null && StringUtils.isNotBlank(wrapperEntity.getOpenid())){
            return new ErrorTip(500, "此卡号已经被" + wrapperEntity.getStudentName() + "绑定！");
        }

        Member member = new Member();
        member.setOpenid(openid);
//        member.setUnionid(unionid);
        member.setMobile(mobile);
        member.setCardNumber(cardNumber);
        member.setNickname(nickname);
        member.setStudentNumber(studentNumber);
        member.setMemberId(wrapperEntity.getMemberId());
        boolean updated = memberService.updateById(member);
        if (!updated) {
            throw new APIException(BizExceptionEnum.BIND_FAIL);
        }

        SuccessTip tip = new SuccessTip();
        tip.setCode(200);
        tip.setMessage("绑定成功");

//        final String studentName = wrapperEntity.getStudentName();
        final String memberNumber = studentNumber;

        executor.execute(new Runnable() {
            @Override
            public void run() {
                Map<String, Object> data = new HashMap<>();
                data.put("first", new TemplateData("恭喜, 您已经成功绑定学号", "#173177"));
                data.put("keyword1", new TemplateData(cardNumber + "(" + memberNumber + ")", "#173177"));
                data.put("keyword2", new TemplateData("绑定之后可直接在微信上登陆", "#173177"));
                data.put("remark", new TemplateData("如需解绑，请访问桌面版进行操作", "#FF0000"));
                WechatUtils.sendTemplateMessage(WechatData.BIND_SUCCESS, openid, null, data);
            }
        });

        return tip;
    }

    /**
     * 课程列表
     */
    @RequestMapping(value = "/courseList", method = RequestMethod.POST)
    public Object courseList(Integer pageNumber, Integer pageSize, Integer memberId) {
        Wrapper<Course> wrapper = new EntityWrapper<>();
        wrapper.ne("lesson_id", 0);
        wrapper.orderBy("week_id", false);
        Page<Course> coursePage = courseService.selectPage(new Page<>(pageNumber, pageSize), wrapper);
        List<Map<String, Object>> resultMap = new ArrayList<>();
        for (Course course : coursePage.getRecords()) {
            Map<String, Object> result = new HashMap<>();
            result.put("courseId", course.getCourseId());
            result.put("lessonId", course.getLessonId());
            result.put("lessonName", course.getLessonName());
            result.put("courseDay", courseService.getWeekDay(course.getWeekId(), course.getCourseDay()));
            Dict dict = new Dict();
            dict.setCode("CLASS_TIME");
            Dict parentDict = dictService.selectOne(new EntityWrapper(dict));
            dict.setCode(null);
            dict.setPid(parentDict.getId());
            dict.setNum(course.getCourseTimeId());
            String name = dictService.selectOne(new EntityWrapper(dict)).getName();
            result.put("courseTime", name);

            resultMap.add(result);
        }

        Subscribe subscribe = new Subscribe();
        subscribe.setMemberId(memberId);
        subscribe.setSubscribeStatus(1);
        List<Subscribe> subscribeList = subscribeService.selectList(new EntityWrapper<>(subscribe));

        Map<Integer, Integer> subscribeMap = new HashMap<>();
        for (int i = 0; i < subscribeList.size(); i++) {
            subscribeMap.put(subscribeList.get(i).getCourseId(), subscribeList.get(i).getSubscribeId());
        }

        for (Map<String, Object> map : resultMap) {
            if (subscribeMap.containsKey(Integer.valueOf(map.get("courseId").toString()))) {
                map.put("subscribeId", subscribeMap.get((Integer)map.get("courseId")));
                map.put("isSubscribe", true);
            } else {
                map.put("isSubscribe", false);
            }
        }

        return resultMap;
    }

    /**
     * 预约课程列表
     */
    @RequestMapping(value = "/subscribeList", method = RequestMethod.POST)
    public Object subscribeList(Integer pageNumber, Integer pageSize, Integer memberId) {
        Subscribe subscribe = new Subscribe();
        subscribe.setMemberId(memberId);
        subscribe.setSubscribeStatus(1);
        EntityWrapper wrapper = new EntityWrapper(subscribe);
        wrapper.orderBy("create_time", false);
        Page<Subscribe> subscribePage = subscribeService.selectPage(new Page<>(pageNumber, pageSize), wrapper);

        List<Map<String, Object>> list = new ArrayList<>();
        for (int i = 0; i < subscribePage.getRecords().size(); i++) {
            Subscribe subscribe1 = subscribePage.getRecords().get(i);
            Map<String, Object> map = new HashMap<>();
            map.put("subscribeId", subscribe1.getSubscribeId());

            Course course = courseService.selectById(subscribe1.getCourseId());
            map.put("courseName", course.getLessonName());
            map.put("courseDay", courseService.getWeekDay(course.getWeekId(), course.getCourseDay()));

            Dict dict = new Dict();
            dict.setCode("CLASS_TIME");
            Dict parentDict = dictService.selectOne(new EntityWrapper(dict));
            dict.setCode(null);
            dict.setPid(parentDict.getId());
            dict.setNum(course.getCourseTimeId());
            String name = dictService.selectOne(new EntityWrapper(dict)).getName();
            map.put("courseTime", name);

            list.add(map);
        }

        return success(list);
    }

    /**
     * 预约
     */
    @RequestMapping(value = "/subscribe", method = RequestMethod.POST)
    public Object subscribe(Integer memberId, Integer courseId) {

        //查询是否已经预约过此课程
        Subscribe selectSubscribe = new Subscribe();
        selectSubscribe.setMemberId(memberId);
        selectSubscribe.setCourseId(courseId);
        selectSubscribe.setSubscribeStatus(1);
        int count = subscribeService.selectCount(new EntityWrapper(selectSubscribe));
        if (count > 0) {
            throw new APIException(BizExceptionEnum.SUBSCRIBED);
        }

        Member member = memberService.selectById(memberId);
        Course course = courseService.selectById(courseId);

        Subscribe subscribe = new Subscribe();
        subscribe.setCourseId(courseId);
        subscribe.setCourseName(course.getLessonName());
        subscribe.setMemberId(memberId);
        subscribe.setStudentName(member.getStudentName());
        subscribe.setStudentNumber(member.getStudentNumber());
        Date now = new Date();
        subscribe.setCreateTime(now);
        subscribe.setSubscribeTime(now);
        subscribe.setUpdateTime(now);
        subscribe.setSubscribeStatus(1);//已预约
        boolean insert = subscribeService.insert(subscribe);
        if (!insert) {
            throw new APIException(BizExceptionEnum.OPERATION_FAIL);
        }

        if(StringUtils.isNotBlank(member.getOpenid())) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> data = new HashMap<>();
                    data.put("first", new TemplateData("尊敬的用户您好，你的课程已经预约成功！"));
                    data.put("keyword1", new TemplateData(course.getLessonName(), "#173177"));
                    data.put("keyword2", new TemplateData(courseService.getWeekDay(course.getWeekId(), course.getCourseDay()), "#173177"));
                    data.put("keyword3", new TemplateData(courseService.getWeekDay(course.getWeekId(), course.getCourseDay()), "#173177"));
                    data.put("keyword4", new TemplateData("学校", "#173177"));
                    data.put("keyword5", new TemplateData("何老师", "#173177"));
                    data.put("remark", new TemplateData("请按时参加！", "#FF0000"));
                    WechatUtils.sendTemplateMessage(WechatData.SUBSCRIBE_SUCCESS, member.getOpenid(), null, data);
                }
            });
        }

        return SUCCESS_TIP;
    }

    /**
     * 取消预约
     */
    @RequestMapping(value = "/cancelSubscribe", method = RequestMethod.POST)
    public Object cancelSubscribe(Integer subscribeId) {

        Date now = new Date();
        Subscribe updateSubscribe = new Subscribe();
        updateSubscribe.setSubscribeId(subscribeId);
        updateSubscribe.setUpdateTime(now);
        updateSubscribe.setSubscribeStatus(0);//取消预约
        boolean updated = subscribeService.updateById(updateSubscribe);
        if (!updated) {
            throw new APIException(BizExceptionEnum.OPERATION_FAIL);
        }

        Subscribe subscribe = subscribeService.selectById(subscribeId);
        Course course = courseService.selectById(subscribe.getCourseId());
        Member member = memberService.selectById(subscribe.getMemberId());

        if(StringUtils.isNotBlank(member.getOpenid())) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    Map<String, Object> data = new HashMap<>();
                    data.put("first", new TemplateData("尊敬的用户您好，您预约的课程已取消！", "#173177"));
                    data.put("keyword1", new TemplateData("课程 - " + course.getLessonName(), "#173177"));
                    data.put("keyword2", new TemplateData(courseService.getWeekDay(course.getWeekId(), course.getCourseDay()), "#173177"));
                    data.put("keyword3", new TemplateData("个人原因", "#173177"));
                    data.put("remark", new TemplateData("您有多余的时间可以预约下一节课程~", "#FF0000"));
                    WechatUtils.sendTemplateMessage(WechatData.SUBSCRIBE_CANCEL, member.getOpenid(), null, data);
                }
            });
        }

        return SUCCESS_TIP;
    }

    /**
     * 修改会员信息
     *
     * @param member
     * @return
     */
    @RequestMapping(value = "/memberUpdate", method = RequestMethod.POST)
    @ResponseBody
    public Object update(@RequestBody Member member) {
        memberService.updateById(member);
        return SUCCESS_TIP;
    }


    /**
     * 修改密码
     * @param memberId
     * @param oldPwd
     * @param newPwd
     * @param confirmPwd
     * @return
     */
    @RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
    @ResponseBody
    public Object update(Integer memberId, String oldPwd, String newPwd, String confirmPwd) {

        Member member = memberService.selectById(memberId);

        if(!ShiroKit.md5(oldPwd, member.getSalt()).equals(member.getPassword())){
            return new ErrorTip(500, "原密码错误");
        }

        if(!newPwd.equals(confirmPwd)){
            return new ErrorTip(500, "两次密码不一致,请重新输入");
        }

        member.setPassword(ShiroKit.md5(newPwd, member.getSalt()));
        memberService.updateById(member);
        return SUCCESS_TIP;
    }
}

