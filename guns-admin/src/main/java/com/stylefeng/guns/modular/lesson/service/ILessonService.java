package com.stylefeng.guns.modular.lesson.service;

import com.stylefeng.guns.modular.system.model.Lesson;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author heshaohua
 * @since 2018-08-29
 */
public interface ILessonService extends IService<Lesson> {

}
