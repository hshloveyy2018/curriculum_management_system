package com.stylefeng.guns.modular.course.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.common.exception.BizExceptionEnum;
import com.stylefeng.guns.core.exception.GunsException;
import com.stylefeng.guns.core.exception.ServiceExceptionEnum;
import com.stylefeng.guns.modular.system.model.Dict;
import com.stylefeng.guns.modular.system.service.IDictService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.Course;
import com.stylefeng.guns.modular.course.service.ICourseService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 课程管理控制器
 *
 * @author fengshuonan
 * @Date 2018-08-22 22:39:40
 */
@Controller
@RequestMapping("/course")
public class CourseController extends BaseController {

    private String PREFIX = "/course/";

    @Autowired
    private ICourseService courseService;

    @Autowired
    private IDictService dictService;

    /**
     * 跳转到课程管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "course.html";
    }

    /**
     * 跳转到添加课程管理
     */
    @RequestMapping("/course_add")
    public String courseAdd() {
        return PREFIX + "course_add.html";
    }


    /**
     * 跳转到修改课程管理
     */
    @RequestMapping("/course_update/{courseId}")
    public String courseUpdate(@PathVariable Integer courseId, Model model) {
        Course course = courseService.selectById(courseId);
        model.addAttribute("item", course);
        LogObjectHolder.me().set(course);
        return PREFIX + "course_edit.html";
    }

    /**
     * 获取课程管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition, Integer courseDay, Integer courseTimeId) {
        List<Map<String, Object>> resultMap = new ArrayList<>();

        EntityWrapper wrapper = new EntityWrapper<>();
        if(StringUtils.isNotBlank(condition)) {
            wrapper.like("course_name", condition);
        }
        if(courseDay != null) {
            wrapper.eq("course_day", courseDay);
        }
        if(courseTimeId != null){
            wrapper.eq("course_time_id", courseTimeId);
        }

        List<Course> courses = courseService.selectList(wrapper);

        for (Course course : courses) {
            Map<String, Object> result = new HashMap<>();
            result.put("courseId", course.getCourseId());
            result.put("courseName", course.getLessonName());
            result.put("courseDay", courseService.getWeekDay(course.getWeekId(), course.getCourseDay()));
            Dict dict = new Dict();
            dict.setCode("CLASS_TIME");
            Dict parentDict = dictService.selectOne(new EntityWrapper(dict));
            dict.setCode(null);
            dict.setPid(parentDict.getId());
            dict.setNum(course.getCourseTimeId());
            String name = dictService.selectOne(new EntityWrapper(dict)).getName();
            result.put("courseTime", name);

            resultMap.add(result);
        }

        return resultMap;
    }

    /**
     * 新增课程管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Course course) {
        Course wrapper = new Course();
        wrapper.setCourseDay(course.getCourseDay());
        wrapper.setCourseTimeId(course.getCourseTimeId());
        int count = courseService.selectCount(new EntityWrapper<>(wrapper));

        if(count > 0){
            throw new GunsException(BizExceptionEnum.COURSE_EXISTED);
        }

        courseService.insert(course);
        return SUCCESS_TIP;
    }

    /**
     * 删除课程管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer courseId) {
        courseService.deleteById(courseId);
        return SUCCESS_TIP;
    }

    /**
     * 修改课程管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Course course) {
        courseService.updateById(course);
        return SUCCESS_TIP;
    }

    /**
     * 课程管理详情
     */
    @RequestMapping(value = "/detail/{courseId}")
    @ResponseBody
    public Object detail(@PathVariable("courseId") Integer courseId) {
        return courseService.selectById(courseId);
    }
}
