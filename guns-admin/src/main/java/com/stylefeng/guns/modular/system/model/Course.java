package com.stylefeng.guns.modular.system.model;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author heshaohua
 * @since 2018-08-22
 */
@TableName("course")
public class Course extends Model<Course> {

    private static final long serialVersionUID = 1L;

    @TableId("course_id")
    private Integer courseId;
    /**
     * 课程时间id
     */
    @TableField("course_time_id")
    private Integer courseTimeId;
    @TableField("course_day")
    private Integer courseDay;
    @TableField("lesson_id")
    private Integer lessonId;
    @TableField("lesson_name")
    private String lessonName;
    @TableField("week_id")
    private Integer weekId;


    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Integer getCourseTimeId() {
        return courseTimeId;
    }

    public void setCourseTimeId(Integer courseTimeId) {
        this.courseTimeId = courseTimeId;
    }

    public Integer getCourseDay() {
        return courseDay;
    }

    public void setCourseDay(Integer courseDay) {
        this.courseDay = courseDay;
    }

    public Integer getLessonId() {
        return lessonId;
    }

    public void setLessonId(Integer lessonId) {
        this.lessonId = lessonId;
    }

    public String getLessonName() {
        return lessonName;
    }

    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }

    public Integer getWeekId() {
        return weekId;
    }

    public void setWeekId(Integer weekId) {
        this.weekId = weekId;
    }

    @Override
    protected Serializable pkVal() {
        return this.courseId;
    }

    @Override
    public String toString() {
        return "Course{" +
        "courseId=" + courseId +
        ", courseTimeId=" + courseTimeId +
        ", courseDay=" + courseDay +
        ", lessonId=" + lessonId +
        ", lessonName=" + lessonName +
        "}";
    }
}
