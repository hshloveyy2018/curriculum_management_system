package com.stylefeng.guns.modular.system.model;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author stylefeng
 * @since 2018-08-22
 */
@TableName("member")
public class Member extends Model<Member> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "member_id", type = IdType.AUTO)
    private Integer memberId;
    /**
     * 学号
     */
    @TableField("card_number")
    private String cardNumber;
    /**
     * 学号
     */
    @TableField("student_number")
    private String studentNumber;
    /**
     * 微信昵称
     */
    private String nickname;
    /**
     * 学生姓名
     */
    @TableField("student_name")
    private String studentName;
    /**
     * 性别 0 - 女, 1 - 男
     */
    private Integer gender;
    /**
     * 专业
     */
    private String major;
    /**
     * 班级
     */
    private String classes;
    private String openid;
    private String unionid;
    /**
     * 手机
     */
    private String mobile;
    private String password;
    private String salt;


    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getClasses() {
        return classes;
    }

    public void setClasses(String classes) {
        this.classes = classes;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Override
    protected Serializable pkVal() {
        return this.memberId;
    }

    @Override
    public String toString() {
        return "Member{" +
        "memberId=" + memberId +
        ", studentNumber=" + studentNumber +
        ", nickname=" + nickname +
        ", studentName=" + studentName +
        ", gender=" + gender +
        ", major=" + major +
        ", classes=" + classes +
        ", openid=" + openid +
        ", unionid=" + unionid +
        ", mobile=" + mobile +
        ", password=" + password +
        ", salt=" + salt +
        "}";
    }
}
