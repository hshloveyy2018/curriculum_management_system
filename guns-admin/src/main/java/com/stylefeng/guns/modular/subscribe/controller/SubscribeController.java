package com.stylefeng.guns.modular.subscribe.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.Subscribe;
import com.stylefeng.guns.modular.subscribe.service.ISubscribeService;

/**
 * 预约管理控制器
 *
 * @author fengshuonan
 * @Date 2018-08-22 22:58:02
 */
@Controller
@RequestMapping("/subscribe")
public class SubscribeController extends BaseController {

    private String PREFIX = "/subscribe/";

    @Autowired
    private ISubscribeService subscribeService;

    /**
     * 跳转到预约管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "subscribe.html";
    }

    /**
     * 跳转到添加预约管理
     */
    @RequestMapping("/subscribe_add")
    public String subscribeAdd() {
        return PREFIX + "subscribe_add.html";
    }

    /**
     * 跳转到修改预约管理
     */
    @RequestMapping("/subscribe_update/{subscribeId}")
    public String subscribeUpdate(@PathVariable Integer subscribeId, Model model) {
        Subscribe subscribe = subscribeService.selectById(subscribeId);
        model.addAttribute("item",subscribe);
        LogObjectHolder.me().set(subscribe);
        return PREFIX + "subscribe_edit.html";
    }

    /**
     * 获取预约管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        EntityWrapper wrapper = new EntityWrapper<>();
        if (StringUtils.isNotBlank(condition)) {
            wrapper.like("student_number", condition)
                    .or().like("student_name", condition)
                    .or().like("course_name", condition);
        }
        return subscribeService.selectList(wrapper);
    }

    /**
     * 新增预约管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Subscribe subscribe) {
        subscribeService.insert(subscribe);
        return SUCCESS_TIP;
    }

    /**
     * 删除预约管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer subscribeId) {
        subscribeService.deleteById(subscribeId);
        return SUCCESS_TIP;
    }

    /**
     * 修改预约管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Subscribe subscribe) {
        subscribeService.updateById(subscribe);
        return SUCCESS_TIP;
    }

    /**
     * 预约管理详情
     */
    @RequestMapping(value = "/detail/{subscribeId}")
    @ResponseBody
    public Object detail(@PathVariable("subscribeId") Integer subscribeId) {
        return subscribeService.selectById(subscribeId);
    }
}
