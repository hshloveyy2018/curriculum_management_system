package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.Lesson;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author heshaohua
 * @since 2018-08-29
 */
public interface LessonMapper extends BaseMapper<Lesson> {

}
