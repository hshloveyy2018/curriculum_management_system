package com.stylefeng.guns.modular.system.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author heshaohua
 * @since 2018-08-22
 */
@TableName("week")
public class Week extends Model<Week> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "week_id", type = IdType.AUTO)
    private Integer weekId;
    @TableField("week_start_day")
    private Date weekStartDay;
    @TableField("week_end_day")
    private Date weekEndDay;


    public Integer getWeekId() {
        return weekId;
    }

    public void setWeekId(Integer weekId) {
        this.weekId = weekId;
    }

    public Date getWeekStartDay() {
        return weekStartDay;
    }

    public void setWeekStartDay(Date weekStartDay) {
        this.weekStartDay = weekStartDay;
    }

    public Date getWeekEndDay() {
        return weekEndDay;
    }

    public void setWeekEndDay(Date weekEndDay) {
        this.weekEndDay = weekEndDay;
    }

    @Override
    protected Serializable pkVal() {
        return this.weekId;
    }

    @Override
    public String toString() {
        return "Week{" +
        "weekId=" + weekId +
        ", weekStartDay=" + weekStartDay +
        ", weekEndDay=" + weekEndDay +
        "}";
    }
}
