package com.stylefeng.guns.modular.course.service.impl;

import com.stylefeng.guns.modular.system.dao.CourseTimeMapper;
import com.stylefeng.guns.modular.system.model.Course;
import com.stylefeng.guns.modular.system.dao.CourseMapper;
import com.stylefeng.guns.modular.course.service.ICourseService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.system.model.CourseTime;
import com.stylefeng.guns.modular.system.model.Dict;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author heshaohua
 * @since 2018-08-22
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements ICourseService {

    private static final String[] week = {"星期一", "星期二", "星期三", "星期四", "星期五"};

    @Autowired
    private CourseTimeMapper courseTimeMapper;

    @Override
    public String getWeekDay(Integer weekId, Integer courseDay) {
        CourseTime courseTime = courseTimeMapper.selectById(weekId);
        try {
            List<String> dayList = getDayList(courseTime.getCourseStartTime(), courseTime.getCourseEndTime());
            return dayList.get(courseDay - 1) + " " + week[courseDay - 1];
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private List<String> getDayList(String courseStartTime, String courseEndTime) throws ParseException {
        List<Date> list = new ArrayList<>();
        Date startDate = DateUtils.parseDate(courseStartTime, "yyyy-MM-dd");
        Date endDate = DateUtils.parseDate(courseEndTime, "yyyy-MM-dd");
        while(!DateUtils.isSameDay(startDate, endDate)){
            list.add(startDate);
            startDate = DateUtils.addDays(startDate, 1);
        }

        list.add(endDate);
        List<String> resultList = new ArrayList<>();
        for (Date d : list){
            resultList.add(DateFormatUtils.format(d, "yyyy-MM-dd"));
        }
        return resultList;
    }

    @Override
    public List<Dict> getWeekDayList() {
        List<Dict> list = new ArrayList<>();
        for (int i = 0; i < week.length; i++) {
            Dict dict = new Dict();
            dict.setCode((i + 1)+ "");
            dict.setName(week[i]);
            list.add(dict);
        }
        return list;
    }
}
