package com.stylefeng.guns.modular.coursetime.service;

import com.stylefeng.guns.modular.system.model.CourseTime;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author heshaohua
 * @since 2018-08-29
 */
public interface ICourseTimeService extends IService<CourseTime> {

}
