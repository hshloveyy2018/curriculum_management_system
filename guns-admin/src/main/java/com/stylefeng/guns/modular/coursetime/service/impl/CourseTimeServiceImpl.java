package com.stylefeng.guns.modular.coursetime.service.impl;

import com.stylefeng.guns.modular.system.model.CourseTime;
import com.stylefeng.guns.modular.system.dao.CourseTimeMapper;
import com.stylefeng.guns.modular.coursetime.service.ICourseTimeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author heshaohua
 * @since 2018-08-29
 */
@Service
public class CourseTimeServiceImpl extends ServiceImpl<CourseTimeMapper, CourseTime> implements ICourseTimeService {

}
