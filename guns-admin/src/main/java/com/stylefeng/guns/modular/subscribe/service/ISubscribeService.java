package com.stylefeng.guns.modular.subscribe.service;

import com.stylefeng.guns.modular.system.model.Subscribe;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author stylefeng
 * @since 2018-08-22
 */
public interface ISubscribeService extends IService<Subscribe> {

}
