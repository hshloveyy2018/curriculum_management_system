package com.stylefeng.guns.modular.subscribe.service.impl;

import com.stylefeng.guns.modular.system.model.Subscribe;
import com.stylefeng.guns.modular.system.dao.SubscribeMapper;
import com.stylefeng.guns.modular.subscribe.service.ISubscribeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2018-08-22
 */
@Service
public class SubscribeServiceImpl extends ServiceImpl<SubscribeMapper, Subscribe> implements ISubscribeService {

}
