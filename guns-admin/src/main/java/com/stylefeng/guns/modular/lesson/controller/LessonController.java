package com.stylefeng.guns.modular.lesson.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.Lesson;
import com.stylefeng.guns.modular.lesson.service.ILessonService;

/**
 * 课程管理控制器
 *
 * @author fengshuonan
 * @Date 2018-08-29 20:58:51
 */
@Controller
@RequestMapping("/lesson")
public class LessonController extends BaseController {

    private String PREFIX = "/lesson/lesson/";

    @Autowired
    private ILessonService lessonService;

    /**
     * 跳转到课程管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "lesson.html";
    }

    /**
     * 跳转到添加课程管理
     */
    @RequestMapping("/lesson_add")
    public String lessonAdd() {
        return PREFIX + "lesson_add.html";
    }

    /**
     * 跳转到修改课程管理
     */
    @RequestMapping("/lesson_update/{lessonId}")
    public String lessonUpdate(@PathVariable Integer lessonId, Model model) {
        Lesson lesson = lessonService.selectById(lessonId);
        model.addAttribute("item",lesson);
        LogObjectHolder.me().set(lesson);
        return PREFIX + "lesson_edit.html";
    }

    /**
     * 获取课程管理列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        Wrapper<Lesson> wrapper = new EntityWrapper<>();
        wrapper.like("lesson_name", condition);
        return lessonService.selectList(wrapper);
    }

    /**
     * 新增课程管理
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Lesson lesson) {
        lessonService.insert(lesson);
        return SUCCESS_TIP;
    }

    /**
     * 删除课程管理
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer lessonId) {
        lessonService.deleteById(lessonId);
        return SUCCESS_TIP;
    }

    /**
     * 修改课程管理
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Lesson lesson) {
        lessonService.updateById(lesson);
        return SUCCESS_TIP;
    }

    /**
     * 课程管理详情
     */
    @RequestMapping(value = "/detail/{lessonId}")
    @ResponseBody
    public Object detail(@PathVariable("lessonId") Integer lessonId) {
        return lessonService.selectById(lessonId);
    }
}
